﻿using System;

namespace LogLib
{
    /// <summary>
    /// Обработчик для события
    /// </summary>
    /// <param name="info">Информация о событии</param>
    public delegate void LogEventHandler(LogEventInfo info);

    /// <summary>
    /// Тип события
    /// </summary>
    public enum LogEventType : byte
    {
        Exception = 0,
        Warning = 1,
        Information = 2,
        Error = 3
    }

    /// <summary>
    /// Структура с информацией о событии
    /// </summary>
    public struct LogEventInfo
    {
        public DateTime WhenHappened { get; set; }
        public object WhereHappened { get; set; }
        public LogEventType WhatHappened { get; set; }
        public object WithWhatHappened { get; set; }
        public object AdditionalInfo { get; set; }
    }
}
